from django.conf.urls import url

from user import views

urlpatterns = [
    url(r'^all/$', views.user_list),
    url(r'^new/$', views.add_user),

    url(r'^hash/all/$', views.hash_list),
    url(r'^hash/new/$', views.add_hash),

    url(r'^password/new/$', views.add_password),
    url(r'^password/all/$', views.password_list),
    url(r'^password/update/$', views.update_password),
    url(r'^password/(.+)$', views.get_password),
]
