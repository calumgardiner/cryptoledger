from django.db import models


class User(models.Model):
    """
    The User model holds the users email and account creation date.
    """
    email = models.EmailField(unique=True)
    '''
    The users email address, is unique across all users
    '''
    created = models.DateTimeField(auto_now=True)
    '''
    The date the users account was created
    '''

    class Meta:
        ordering = ('created',)


class HashType(models.Model):
    """
    The HashType model holds a hash type description (e.g. bcrypt, PBKDF2withHMacSha1 etc). Password hashes are
    HashType dependant to ensure that migration to new hashing technology can be done without issue.
    """
    hash_name = models.CharField(unique=True, max_length=50)
    '''
    The hash name of this hash type
    '''

    class Meta:
        ordering = ('hash_name',)


class UserPassword(models.Model):
    """
    The UserPassword model holds a users hashed password along with the hashing algorithm used to hash it.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    '''
    The user whose password this is
    '''
    password = models.CharField(max_length=60)
    '''
    The users hashed password
    '''
    hash_type = models.ForeignKey(HashType, on_delete=models.DO_NOTHING)
    '''
    The hash type used to create the password
    '''

    class Meta:
        ordering = ('user',)
