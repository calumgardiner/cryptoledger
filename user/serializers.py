from rest_framework import serializers

from user.models import User, HashType, UserPassword


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for the User model
    """
    id = serializers.IntegerField(read_only=True)
    '''
    The User database id
    '''
    email = serializers.EmailField()
    '''
    The users email address, is unique across all users
    '''
    created = serializers.DateTimeField(read_only=True)
    '''
    Account creation date
    '''

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """
        return User.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `user` instance, given the validated data.
        """
        instance.email = validated_data.get('email', instance.email)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('id', 'email', 'created')


class HashTypeSerializer(serializers.ModelSerializer):
    """
    Serializer for the HashType model
    """
    id = serializers.IntegerField(read_only=True)
    '''
    The HashType database id
    '''
    hash_name = serializers.CharField(read_only=False)
    '''
    The hash name
    '''

    def create(self, validated_data):
        """
        Create and return a new `HashType` instance, given the validated data.
        """
        return HashType.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `HashType` instance, given the validated data.
        """
        instance.hash_name = validated_data.get('hash_name', instance.hash_name)
        instance.save()
        return instance

    class Meta:
        model = HashType
        fields = ('id', 'hash_name')


class UserPasswordSerializer(serializers.ModelSerializer):
    """
        Serializer for the UserPassword model
    """
    id = serializers.IntegerField(read_only=True)
    '''
    The user password database id
    '''
    user = serializers.SlugRelatedField(many=False, slug_field='email', queryset=User.objects.all())
    '''
    The user who's password this is
    '''
    password = serializers.CharField()
    '''
    The user's hashed password
    '''
    hash_type = serializers.SlugRelatedField(many=False, slug_field='hash_name', queryset=HashType.objects.all())
    '''
    The hash type used to hash this password
    '''

    def create(self, validated_data):
        """
        Create and return a new `UserPassword` instance, given the validated data.
        """
        return UserPassword.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `UserPassword` instance, given the validated data.
        """
        instance.user = validated_data.get('user', instance.user)
        instance.password = validated_data.get('password', instance.password)
        instance.hash_type = validated_data.get('hash_type', instance.hash_type)
        instance.save()
        return instance

    class Meta:
        model = UserPassword
        fields = ('id', 'user', 'password', 'hash_type')
