from abc import ABC, abstractmethod

import bcrypt

"""
Hashing utilties. Provides an abstract class PasswordDigest which should define the 'publically' available methods for 
each implementation. Currently provides a bcrypt implementation. When bcrypt becomes obsolete implement and new digest.

The passwords table will contain the hash_name property and the Digests dictionary defined here can be used effectively
as a strategy factory, to retrieve the correct digest strategy based on the hash_name.

"""

_UTF8 = 'utf-8'


class PasswordDigest(ABC):
    """
    Abstract PasswordDigest class, provides the expected methods for digest implementations.
    """
    hash_name = None

    @abstractmethod
    def hash_password(self, plain_text):
        '''
        Hash the plain text password and return a string representation of the resulting hash. The hash
        representation returned there should be directly (without type conversion) passable to the compare method.
        :param plain_text: the plain text password
        :return: The resulting password hash in a string
        '''
        return NotImplemented

    @abstractmethod
    def compare_password(self, hash, attempt):
        '''
        Compare the stored hash with the specified password attempt. Returns boolean.
        :param hash: the stored hash from the datbase
        :param attempt: the password attempt
        :return: boolean True/False
        '''
        return NotImplemented


class BCryptDigest(PasswordDigest):
    """
    BCrypt implementation of PasswordDigest. https://en.wikipedia.org/wiki/Bcrypt
    """
    hash_name = 'bcrypt'
    __rounds = 15

    def hash_password(self, plain_text):
        plain_bytes = bytes(plain_text, _UTF8)
        hashed = bcrypt.hashpw(plain_bytes, bcrypt.gensalt(self.__rounds))
        return str(hashed, _UTF8)

    def compare_password(self, hash, attempt):
        stored_hash = bytes(hash, _UTF8)
        attempt_bytes = bytes(attempt, _UTF8)
        return bcrypt.checkpw(attempt_bytes, stored_hash)


Digests = {BCryptDigest.hash_name: BCryptDigest()}
'''
    Digests dictionary of hash names to the implementations
'''
