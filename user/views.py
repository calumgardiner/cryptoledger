from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from user.models import User, HashType, UserPassword
from user.serializers import UserSerializer, UserPasswordSerializer, HashTypeSerializer


@csrf_exempt
def user_list(request):
    """
    List all users.
    """
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({}, status=501)


@csrf_exempt
def add_user(request):
    """
    Add a user.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    return JsonResponse({}, status=501)


@csrf_exempt
def hash_list(request):
    """
        List all hashes.
        """
    if request.method == 'GET':
        users = HashType.objects.all()
        serializer = HashTypeSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({}, status=501)


@csrf_exempt
def add_hash(request):
    """
    Add a hash.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = HashTypeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    return JsonResponse({}, status=501)


@csrf_exempt
def add_password(request):
    """
    Add a user password.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserPasswordSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    return JsonResponse({}, status=501)


@csrf_exempt
def password_list(request):
    """
    List all passwords.
    """
    if request.method == 'GET':
        users = UserPassword.objects.all()
        serializer = UserPasswordSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({}, status=501)


@csrf_exempt
def get_password(request, email):
    """
    Get password for user.
    """
    if request.method == 'GET':
        user = User.objects.get(email=email)
        password = UserPassword.objects.get(user=user.id)
        serializer = UserPasswordSerializer(password, many=False)
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({}, status=501)


@csrf_exempt
def update_password(request):
    """
    Update a user password.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        user = User.objects.get(email=data['user'])
        password = UserPassword.objects.get(user=user.id)
        serializer = UserPasswordSerializer(password, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    return JsonResponse({}, status=501)
