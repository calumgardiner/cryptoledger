from django.test import TestCase

from user.hashing import Digests


class TestBCryptDigest(TestCase):
    """
    Test the BCrypt PasswordDigest implementation
    """

    def setUp(self):
        self.plain_text = 'test_password$$%%'
        self.digest = Digests['bcrypt']

    def test_hash_compare(self):
        '''
        Test that hashing a password then checking it later with the same plain text returns true
        '''
        hashed_password = self.digest.hash_password(self.plain_text)
        is_correct = self.digest.compare_password(hash=hashed_password, attempt=self.plain_text)
        self.assertTrue(is_correct)

    def test_hash_fails_incorrect_password(self):
        '''
        Test that hashing a password then comparing to the wrong password returns false
        '''
        not_password = 'not_password'
        hashed_password = self.digest.hash_password(self.plain_text)
        is_incorrect = self.digest.compare_password(hash=hashed_password, attempt=not_password)
        self.assertFalse(is_incorrect)
